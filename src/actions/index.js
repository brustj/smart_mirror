const fetch = require('node-fetch');

export const SET_NEWS = 'SET_NEWS';
export const SET_WEATHER = 'SET_WEATHER';

export const getNews = () => dispatch => {
  return fetch(`https://api.nytimes.com/svc/topstories/v2/home.json?api-key=47HgBosqYrMYAvYgbmg2UPECN5Egjn7V`)
    .then(res => res.json())
    .then(data => {
      console.log(data);
      const sports = data.results.filter(result => result.section === 'Sports').splice(0, 1)[0];
      const us = data.results.filter(result => result.section === 'U.S.').splice(0, 1)[0];
      const world = data.results.filter(result => result.section === 'World').splice(0, 1)[0];

      dispatch(setNews({
        sports,
        us,
        world
      }));
    })
    .catch(err => {
      console.log( 'nytimes service error', err );
    });
};

export const setNews = sections => {
  return {
    type: SET_NEWS,
    sections
  }
};

export const getWeather = () => dispatch => {
  // open weather api
  // https://api.openweathermap.org/data/2.5/weather?q=seattle&appid=77c140132920721921f1b4f7b311b2f9&units=imperial

  // dark sky
  // https://cors-anywhere.herokuapp.com/https://api.darksky.net/forecast/8fecbe8fcb68c16f58108fab1857698c/47.606209,-122.332069
  // https://6xjj7qqgz2.execute-api.us-east-1.amazonaws.com/beta/47.606209,-122.332069
    
  return fetch(`https://cors-anywhere.herokuapp.com/https://api.darksky.net/forecast/8fecbe8fcb68c16f58108fab1857698c/47.606209,-122.332069`)
    .then(res => res.json())
    .then(data => {
      dispatch(setWeather({
        chance: data.currently.precipProbability,
        description: data.minutely.summary,
        hourly: data.hourly.data,
        icon: data.currently.summary.replace(/ /g, ''),
        temp: Math.floor(data.currently.temperature)
      }));
    })
    .catch(err => {
      console.log( 'weather service error', err );
    });
};

export const setWeather = weather => {
  return {
    type: SET_WEATHER,
    weather
  }
};