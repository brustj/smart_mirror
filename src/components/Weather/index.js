import React, { Component } from 'react';
import { connect } from 'react-redux';

import './index.scss';

import clearDay from './clear-day.svg';
import clearNight from './clear-night.svg';
import cloudy from './cloudy.svg';
import drizzle from './drizzle.svg';
import rain from './rain.svg';
import thunderstorms from './thunderstorms.svg';
import partlyCloudyDay from './partly-cloudy-day.svg';
import partlyCloudyNight from './partly-cloudy-night.svg';
import foggy from './foggy.svg';

const moment = require('moment');
const icons = {
	Clear: clearDay,
	Sunny: clearDay,
	Cloudy: cloudy,
	MostlyCloudy: cloudy,
	Drizzle: drizzle,
	LightRain: drizzle,
	Rain: rain,
	Thunderstorms: thunderstorms,
	Overcast: cloudy,
	PartlyCloudy: partlyCloudyDay,
	PossibleDrizzle: drizzle,
	Foggy: foggy,
	cloudy: cloudy,
	'partly-cloudy-day': partlyCloudyDay,
	'partly-cloudy-night': partlyCloudyNight,
	'clear-day': clearDay,
	'clear-night': clearNight,
	'rain': rain,
	'fog': foggy,
	'clear': clearDay,
	'sunny': clearDay,
	'cloudy': cloudy,
	'overcast': cloudy,
};

class Weather extends Component {
	constructor(props) {
		super(props);

		this.formatHour = this.formatHour.bind(this);

		this.state = {
			chance: 0,
			description: 'Loading...',
			hourly: [],
			icon: '',
			temp: 0
		};
	}
	formatHour(time) {
		return moment.unix(time).format('ha');
	}
	render() {
		const {
			// chance,
			description,
			hourly,
			icon,
			temp
		} = this.props.weather;

		return (
			<div className='weather'>
				<div className='tempIconWrap'>
					<h1>{temp}<span>o</span></h1>

					{icons[icon] ?
						<img src={icons[icon] || icons.Sunny} height='90' width='auto' alt={icon} />
						:
						<p>Missing Icon:<br />{icon}</p>
					}
				</div>

				<h2>{description}</h2>

				{/* <h3>{chance * 100}%</h3> */}

				<div className='hourlyWrap'>
					{hourly ? hourly.slice(1, 9).map((hour, index) => (
						<div className='hourly' key={index}>
							<p className='hour'>{this.formatHour(hour.time)}</p>

							<img src={icons[hour.icon]} height='24' width='auto' alt={hour.icon} />

							<p className='temp'>{Math.floor(hour.temperature)}<span>o</span></p>
						</div>
					)
					) : null}
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	weather: state.weather
});

export default connect(mapStateToProps)(Weather);