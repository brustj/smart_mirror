import React, { Component } from 'react';
import io from 'socket.io-client';

import './index.scss';

export default class System extends Component {
	constructor(props) {
		super(props);

		this.state = {
			cpu: 0,
			temp: 0
		}
	}
	componentDidMount() {
		const socket = io('http://192.168.50.26:8080');

		socket.on('smart_mirror_system', data => {
			console.log( `io system message recieved`, data);

			this.setState({...data});
		});
	}
  render() {
		const {
			cpu,
			temp
		} = this.state;

    return (
			<div className='system'>CPU: {Math.round(cpu)}%, Temp: {temp}</div>
    );
  }
}