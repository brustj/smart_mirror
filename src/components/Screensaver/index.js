import React, { Component } from 'react';

import './index.scss';

export default class DateTime extends Component {
	constructor(props) {
		super(props);

		this.drawInterval = null;
		this.boxWidth = 12;
		this.boxHeight = 12;
		this.dx = 4;
		this.dy = -6;

		this.draw = this.draw.bind(this);
		this.drawBall = this.drawBall.bind(this);
	}
	componentDidMount() {
		this.canvas = document.querySelector('#ssCanvas');
		this.ctx = this.canvas.getContext('2d');
		this.x = this.canvas.width / 2;
		this.y = this.canvas.height / 2;
	}
	componentDidUpdate(prevProps) {
		if (!prevProps.show && this.props.show) {
			this.drawInterval = window.setInterval(this.draw, 10);
		}

		if (prevProps.show && !this.props.show) {
			window.clearInterval(this.drawInterval);
		}
	}
	draw() {
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
		
		this.drawBall();
	
		if (this.x + this.dx > this.canvas.width - this.boxWidth || this.x + this.dx < this.boxWidth) {
			this.dx = -this.dx;
		}
	
		if (this.y + this.dy > this.canvas.height - this.boxHeight || this.y + this.dy < this.boxHeight) {
			this.dy = -this.dy;
		}

		this.x += this.dx;
		this.y += this.dy;
	}
	drawBall() {
		this.ctx.beginPath();
		this.ctx.fillStyle = '#FFF';
		this.ctx.fillRect(this.x, this.y, this.boxWidth, this.boxHeight);
		this.ctx.closePath();
	}
  render() {
		const { show } = this.props;

    return (
      <div className='screensaver' data-show={show}>
				<canvas
					id='ssCanvas'
					width={`${window.innerWidth}px`}
					height={`${window.innerHeight}px`} />
			</div>
    );
  }
}