import React, { Component } from 'react';
import { connect } from 'react-redux';

import './index.scss';

class News extends Component {
	constructor(props) {
		super(props);
	}
  render() {
		const { news } = this.props;
		const story = news.sports || news.us || news.world || {};

    return (
      <div className='news'>
				<p>{story.title || 'Loading news...'}</p>
				<p className='abstract'>{story.abstract || ''}</p>
			</div>
    );
  }
}

const mapStateToProps = state => ({
	news: state.news
});

export default connect(mapStateToProps)(News);