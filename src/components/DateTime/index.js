import React, { Component } from 'react';

import './index.scss';

const moment = require('moment');

export default class DateTime extends Component {
	constructor(props) {
		super(props);

		this.getDateTime = this.getDateTime.bind(this);

		this.state = {
			date: 'World',
			day: 'Hello',
			time: '12:00 AM'
		};
	}
	componentDidMount() {
		this.getDateTime();

		window.setInterval(this.getDateTime, 1000);
	}
	getDateTime() {
		const now = moment();
		const date = now.format('MMMM Do');
		const day = now.format('dddd');
		const time = now.format('h:mm');

		this.setState({
			date,
			day,
			time
		});
	}
  render() {
		const {
			date,
			day,
			time
		} = this.state;

    return (
      <div className='dateTime'>
				<h1>{time}</h1>
				<h2>{day}<br />{date}</h2>
			</div>
    );
  }
}