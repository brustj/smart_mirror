import { SET_WEATHER } from 'actions';

export const weather = (state = {}, action) => {
  switch (action.type) {
    case SET_WEATHER:
      return Object.assign({}, {...action.weather});
    default:
      return state;
  }
};
