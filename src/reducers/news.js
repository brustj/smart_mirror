import { SET_NEWS } from 'actions';

export const news = (state = {}, action) => {
  switch (action.type) {
    case SET_NEWS:
      return Object.assign({}, {...action.sections});
    default:
      return state;
  }
};
