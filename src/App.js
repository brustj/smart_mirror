import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';

import Weather from './components/Weather';
import News from './components/News';
import DateTime from './components/DateTime';
import System from './components/System';

import { getNews, getWeather } from 'actions';

import 'App.scss';

class App extends Component {
	componentDidMount() {
		const { dispatch } = this.props;

		window.setInterval(() => {
			dispatch(getNews());
			dispatch(getWeather());
		}, 60000*5); // refresh weather and news every 5 min

		dispatch(getWeather());
		dispatch(getNews());
	}
	render() {
		return (
			<Fragment>
				<div className='app'>
					<div className='section top'>
						<Weather />
						<DateTime />
					</div>

					<div className='section bottom'>
						<News />
						<System />
					</div>
				</div>
			</Fragment>
		);
	}
}

export default connect(null)(App);