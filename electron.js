const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
let mainWindow;

const createWindow = () => {
	const electronOptions = {
		width: 800,
		height: 600,
		x: 0,
		y: 0,
		darkTheme: true,
		webPreferences: {
			nodeIntegration: false,
			zoomFactor: 1.0
		},
		backgroundColor: '#000',
		fullscreen: true,
		autoHideMenuBar: true
	};
	
	mainWindow = new BrowserWindow(electronOptions);
	
	mainWindow.loadURL(`http://localhost:3000/`);
	
	mainWindow.on('closed', () => {
		mainWindow = null;
	});
};

app.on('ready', () => {
	console.log( 'Launching application.' );
	createWindow();
});

app.on('window-all-closed', () => {
	mainWindow = null;
});

app.on('activate', () => {
	// On OS X it's common to re-create a window in the app when the
	// dock icon is clicked and there are no other windows open.
	if (mainWindow === null) {
		createWindow();
	}
});

app.on('before-quit', event => {
	console.log( 'Shutting down server...' );
	event.preventDefault();
	setTimeout(() => { process.exit(0); }, 3000); // Force-quit after 3 seconds.
	process.exit(0);
});